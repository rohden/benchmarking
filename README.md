# Benchmarking

Benchmarking structural variant calling tools for microgenomes


## Using this Package:

### Prerequisites:

 - The following programs must be installed along with their prerequisites:
      - Alignment Tools:
          - [BWA](http://bio-bwa.sourceforge.net/)
          - [Speedseq](https://github.com/hall-lab/speedseq)
          - [Bowtie2](http://bowtie-bio.sourceforge.net/bowtie2/index.shtml)
      - SV Calling Tools:
          - [LUMPY](https://github.com/arq5x/lumpy-sv) (Note: LUMPY comes with Speedseq)
          - [Gustaf](https://github.com/seqan/seqan/tree/master/apps/gustaf)
          - [Breakdancer Max](http://breakdancer.sourceforge.net/)
          - [CNVNator](https://github.com/abyzovlab/CNVnator)
          - [SVDetect](http://svdetect.sourceforge.net/Site/Home.html)
      - Other software:
          - [Stellar](https://github.com/seqan/seqan/tree/master/apps/stellar)
          - [Samtools](https://github.com/samtools/)
          - [Python 3.6+](https://www.python.org/)
          - [Pyinstaller](https://www.pyinstaller.org/)
          - [R](https://www.r-project.org/)
          - [RSVSim](https://github.com/Budheimer/RSVSim)
      - Debian 9 (Stretch) 
          - Other Linux distros may also work but were not tested


### Environment:

This software utilizes these environment variables:
 - `BENCHMARKING_HOME` -- the root directory of this repository  
   e.g. this file should have the path: `${BENCHMARKING_HOME}/README.md`
 - `SVD_BIN` -- the SVDetect binary located at `SVDetect/bin/SVDetect`
 - `SVD_PP`  -- the SVDetect preprocessing script located at `SVDetect/scripts/BAM_preprocessingPairs.pl`
 - `ART_BIN` -- the ART Illumina binary located at `art_bin_MountRainier/art_illumina`
 - `BDM_BIN` -- the Breakdancer Max binary located at `breakdancer/build/bin/breakdancer-max`
 - `BDM_B2C` -- the Breakdancer Max bam2cfg script located at `breakdancer/perl/bam2cfg.pl`
 - `CNVN_BIN` -- the CNVNator binary located at `CNVnator/src/cnvnator`
 - `CNVN_VCF` -- the CNVNator vcf conversion script located at `CNVnator/cnvnator2VCF.pl`
 - `GUSTAF_BIN` -- the Gustaf binary located at `sequan/bin/gustaf`
 - `GUSTAF_MJ` -- the Gustaf preprocessing script located at `sequan/bin/gustaf_mate_joining`
 - `STELLAR_BIN` -- the Stellar binary located at `sequan/bin/stellar`
 - `LUMPY_BIN` -- the LUMPY express binary located at `speedseq/lumpyexpress`


### Path:

The following programs are expected to be in the `${PATH}`:
- `python3.6`
- `pyinstaller`
- `samtools`
- `bwa`
- `bowtie2`
- `speedseq`
- `make`
- `R`


### Running the code:
All paths below will be relative to `${BENCHMARKING_HOME}` as defined in the `Environment` section. The example will use `ecoli` but this can easily replaced with any of the other genomes that are provided. For using another genome that isn't provided, see the `Using other Genomes` section below.

1. Produce the altered genomes:
    1. Run the `Makefile` located at `code/Makefile` using:  
       `make bin`
    2. Edit the configuration files located at `genomes/variants/x/y/z.toml` for the variants       according to your needs 
       - `x` in the path is the genome e.g. `ecoli`
       - `y` in the path is the data-set name e.g. `all`
       - `z` in the path is the data-set configuration file (there can be multiple) e.g. `all_1.toml all_2.toml` etc.
    3. Create the RSVSim scripts according to your configurations:  
       `make scripts environment_file=${BENCHMARKING_HOME}/code/configs/simulate_sv_env_example.toml genome_target=ecoli`
    4. Run the RSVSim scripts:  
       `make run genome_target=ecoli`
2. Preparing the input data:
    1. Run the alignment script located at `code/scripts/alignments.sh` (this will take some time and use a __significant__ amount of memory/cpu/disk space):  
       `sh ${BENCHMARKING_HOME}/code/scripts/alignments.sh -v -bwa -bt2 -ss -gr -ec --index-bwa-ref --index-bowtie2-ref --index-speedseq-ref`
       - Run `sh ${BENCHMARKING_HOME}/code/scripts/alignments.sh --help` for details on the arguments
    2. Sort the alignments:
       `sh ${BENCHMARKING_HOME}/code/scripts/sort_alignments.sh -ec`
       - Run `sh ${BENCHMARKING_HOME}/code/scripts/sort_alignments.sh --help` for details on the arguments
3. Run the benchmarks:
    1. The execution is divided into three scripts all located in `code/scripts` `run_all.sh`, `run_gustaf.sh`, and `run_svd.sh`. They can be run in any order and all take the same arguments:  
      `sh ${BENCHMARKING_HOME}/code/scripts/run_*.sh -ec`
4. Run the analysis code:
    1. The true positives need to first be moved to the correct place, there is no script to do this automatically unfortunately. The true positve information is located in `genomes/altered/x/y/z.csv` and `genomes/temp/x/y/z.csv`. The files should be moved to `data/true_positives/x/y/z.csv` and `data/true_positives/x/y/z2.csv` (Note: one of the two must be renamed as they will all share the same name, the new name should have the same prefix e.g. `deletions.csv` and `deletions_large.csv`)
       - `x` in the path is the genome e.g. `ecoli`
       - `y` in the path is the data-set name e.g. `all`
       - `z` in the path is the kind of variant e.g. `deletions` 
    2. Edit the provided configuration file according to your needs and the included instructions located at `code/evaluation/ecoli_stats.toml`. 
    3. Run the result analysis code: 
       `python3.6 result_analysis -v -c ${BENCHMARKING_HOME}/code/evaluation/ecoli_stats.toml`
    4. The final output will be a csv formatted file in the results folder for each tool (e.g. `bdm.csv` `gustaf.csv` etc.) that contains the Precision and Recall values for all data sets with each of the given thresholds (from the configuration file) as well as how many calls each tool made and how many calls were expected (the number of true positives). 

## TODO

### Code Improvements:

 - There is a typo in many files where `mmaripuladis` was used instead of `mmaripaludis`, the 
   files/directories need to be updated as well

 - It would be nice to have a program that executes all the steps in `Running the Code` automatically

 - Bowtie2 is unnecessary and should be removed from the alignment and processing steps;  
   BWA might also be unnecessary as speedseq appears to be using BWA

 - All the bash scripts should be rewritten in something nicer like Python

 - The bash scripts use the --sam key to detect a sam file that needs to be compressed to bam format maybe the files should just be passed with either --sam and --bam keys rather than --alignment-file

 - Having all logs in a single place might be a lot easier to work with

 - The true-positives are scattered all over the place, a script should move them to a central location for reading

 - All timing information goes into the log files, this is less than ideal, they should be written into a csv file or something for easy analysis

 - The directory structure got out-of-hand complex, this should be updated  
   Perhaps tagging extensions on the file names for each step  will be cleaner? e.g.: a.txt, a.txt.b, a.txt.b.c, etc.

 - The disk space being used is pretty large, maybe there are unnecessary files being generated that can be removed without endangering reproduceability? A good place to start is definitely the alignments, 2/3 are probably not needed.


### Analysis:

 - How are precision and recall effected when the union or in tersection of multiple tools' results are used?

 - What is a realistic number of SVs to simulate and what sizes should be used?

 - How do different number of SVs impact the precision and recall for the tools?

