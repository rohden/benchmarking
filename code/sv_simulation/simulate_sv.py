from configparser import ConfigParser
from argparse import ArgumentParser
from toml import loads, TomlDecodeError
from sys import exit
from numpy.random import randint
from utility import log_message, create_dir_structure, relative_to_absolute, relatives_to_absolutes, is_file

'''
# 2 deletions (5bp), 2 insertions (5bp),2 inversions (3bp), 1 tandem duplication (4bp), 1 translocations
sim = simulateSV(output=NA, genome=genome, dels=2, ins=2, invs=2, dups=1, trans=1, sizeDels=5, sizeIns=5, sizeInvs=3, sizeDups=4, maxDups=3, percCopiedIns=0.5, bpSeqSize=10)
sim = simulateSV(output=NA, genome=genome, dels=1, sizeDels=5, bpFlankSize=10, percSNPs=0.25, indelProb=1, maxIndelSize=3, bpSeqSize=10);

sim = simulateSV(output=".", genome, chrs, dels=0, ins=0, invs=0, dups=0, trans=0, size, sizeDels=10, sizeIns=10, 
           sizeInvs=10, sizeDups=10, regionsDels, regionsIns, regionsInvs, regionsDups, regionsTrans, maxDups=10, 
           percCopiedIns=0, percBalancedTrans=1, bpFlankSize=20, percSNPs=0, indelProb=0, maxIndelSize=10, 
           repeatBias=FALSE, weightsMechanisms, weightsRepeats, repeatMaskerFile, bpSeqSize=100, random=TRUE, seed);
'''

verbose = False

log_info = None
log_error = None
log_crit = None


def to_vector_string(vals):
    """
    Turns the given integer array into an R vector string.
    :param vals: array of values to insert into vector string
    :returns: A string containing the vector descriptor in R syntax or "0"
    """

    vector_str = "0"

    if len(vals) > 0:
        proto = "c({}" + "".join([", {}" for _ in range(len(vals) - 1)]) + ")"

        vector_str = proto.format(*vals)
    
    return vector_str



def random_sizes(number, _min, _max):
    """
    Generates a list of random numbers of given size in the given range
    and then turs them into a R vector string.
    :param number: number of elements to generate
    :param _min: lower limit (inclusive)
    :param _max: upper limit (exclusive)
    :returns: A string containing a vector descriptor in R syntax
    :except ValueError: thrown if either _min or _max is negative
    """

    if _min < 0 or _max < 0:
        raise ValueError(f"Random range given '[{_min},{_max}]' is invalid (negative values)")

    if _min > _max:
        log_error(f"Random range given '[{_min},{_max}]' appears to be reversed, using '[{_max},{_min}]' instead", None)
        _min, _max = _max, _min

    rands = randint(low=_min, high=_max, size=number)

    return to_vector_string(rands)


def prepare_size(sizes, number):
    """
    Turns the given sizes array into either a number or vector string.
    :param sizes: array containing either a single value or number values
    :param number: number of variants being generated
    :returns: A string containing a number or a vector descriptor in R syntax
    :except ValueError: thrown if sizes is not either 1 or number elements long
    """
    if len(sizes) == 1:
        return str(sizes[0])

    elif len(sizes) == number:
        return to_vector_string(sizes)

    else:
        raise ValueError("Size must be an array of 1 item or of N items.")

    return None


def parse_config(file_path):
    """
    Parses the given file to extract a configuration dictionary from it.
    :param file_path: path to a file formatted according to toml 0.4.0
    :returns: a configuration dictionary or None
    """

    config = None

    try:
        with open(file_path) as conf_file:
            config = loads(conf_file.read())

    except TomlDecodeError as e:
        log_crit('Could not parse configuration file due to invalid TOML', e)
        exit(1)

    except IOError as e:
        log_crit('Could not open configuration file', e)
        exit(1)

    return config


def setup(args):
    """
    Parses configuration file and sets up logging.
    :param args: parsed command line arguments
    :returns: the configuration parsed from the user-specified file
    """
    
    global log_info
    global log_error
    global log_crit

    # use verbose = True until config parsing is done
    log_crit = lambda m, e: log_message(m, 'c', True, e)

    conf = parse_config(args.conf)

    # whether or not to print messages to stdout
    # the command line argument '-v' overrides the configuration file setting
    # if '-v' is not used the configuration file setting is used
    verbose = args.verbose if args.verbose is not None else False if 'verbose' not in conf['setup'] else conf['setup']['verbose']

    # these avoid having to pass all arguments all the time
    log_info = lambda m: log_message(m, 'i', verbose, None)
    log_error = lambda m, e: log_message(m, 'e', verbose, e)
    log_crit = lambda m, e: log_message(m, 'c', verbose, e)

    if conf is None:
        log_crit(f"The configuration file ('{args.conf}') was not parsed correctly.", None)
        exit(1)

    # add custom environment for path substitution
    # this allows running this code on machine X while utilizing the env of machine Y
    if args.env is not None:
        conf.update(parse_config(args.env))
    else:
        conf['env'] = None

    return conf


def main():
    parser = ArgumentParser(prog="SV simulation pipeline", description="Simulates structural variants in a fasta-formatted reference genome using RSVSim.")
    parser.add_argument("--conf", "-c", required=True, help="path to the (TOML-formatted) configuration file", metavar="configuration path") 
    parser.add_argument("--verbose", "-v", help="print status messages to stdout", action="store_true") 
    parser.add_argument("--env", help="path to toml file containing custom environment variables to use in script paths") 
    args = parser.parse_args()

    # extract configurations from the given file
    conf = setup(args)

    # write the r scripts according to the TOML instructions
    build_r_scripts(conf)

    return None


def build_r_scripts(config):
    # keys that are expected to be in the configuration file
    expected = set(['deletions', 'insertions', 'inversions', 'duplications', 'translocations'])

    # prototype for the simulateSV call
    # this should be on one line but isn't for readability
    # the replace calls make sure that it will be written as 1 line into the file
    function_proto = """
                        simulateSV(output=\"{output}\", genome=\"{genome}\", dels={n_deletions}, ins={n_insertions}, invs={n_inversions}, dups={n_duplications},
                        trans={n_translocations}, sizeDels={size_deletions}, sizeIns={size_insertions}, sizeInvs={size_inversions}, sizeDups={size_duplications},
                        random=TRUE, seed={seed}, verbose={verbose})
                     """.replace('\n', '').replace('\t','').replace(' ', '').replace(",", ", ")
    
    # extract all paths and resolve them to abolsute paths for convenience
    script_name, genome_file, output_dir = config['setup']['files']

    script_name = relative_to_absolute(script_name)

    # use custom environment for file paths in the script
    genome_file, output_dir = relatives_to_absolutes([genome_file, output_dir], config['env'])

    # create the folder structure for the script to avoid open throwing an exception
    create_dir_structure(script_name)

    script_exists = is_file(script_name)

    with open(script_name, 'a+') as r_script:
        script_conf = {
            'output': output_dir,
            'genome': genome_file,
            'verbose': "TRUE" if config['setup']['verbose'] else "FALSE",
            'seed': config['setup']['seed']
        }

        # iterate over all groups to process them into the script_conf
        # any missing ones will be post-processed and malformed ones ignored
        for k, v in config['variants'].items():
            script_conf[f"n_{k}"] = v['number']

            # translocations do not require any additional configurations
            if k != 'translocations':
                try:
                    s = prepare_size(v['sizes'], v['number'])

                # either sizes or random_size_range must be provided in config
                except KeyError as _:
                    log_info(f"Sizes for '{k}' not found, using random instead.")

                    try:
                        s = random_sizes(v['number'], v['random_size_range'][0], v['random_size_range'][1])

                    # both sizes and random_size_range are missing
                    except KeyError as e:
                        log_error(f"No size configuration found for {k}! Skipping it", e)
                        s = "0"

                    # random_sizes throws this when the range includes negatives
                    except ValueError as e:
                        log_error(f"Random size range for {k} is invalid, skipping it", e)

                # prepare_sizes throws this when len(sizes) != 1 and len(sizes) != number
                except ValueError as e:
                    log_error(f"Size array for {k} is malformed, skipping it", e)
                    s = "0"

                script_conf[f'size_{k}'] = s
            

        # verify that all keys were present and set any missing keys to 0
        for k in expected:
            if f"n_{k}" not in script_conf:
                log_info(f"Number configuration for {k} not found, setting it to 0")
                script_conf[f"n_{k}"] = 0

            if f"size_{k}" not in script_conf and k != 'translocations':
                log_info(f"Size configuration for {k} not found, setting it to 0")
                script_conf[f"size_{k}"] = 0

        # only add import at the top of the file
        if not script_exists:
            r_script.write(f"library(RSVSim)\n")

        r_script.write(f"{function_proto.format(**script_conf)}\n")


    return None


if __name__ == "__main__":
    main()
