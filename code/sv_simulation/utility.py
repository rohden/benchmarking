import logging
from pathlib import Path
import os
import re

def log_message(msg, level, verbose=False, err=None):
    if verbose:
        print(f"{msg}\n")
        if err is not None:
            print(str(err))
    
    f = None
    if level == 'i':
        f = logging.info
    elif level == 'c':
        f = logging.critical
    elif level == 'e':
        f = logging.error

    if f is not None:
        f(f"{msg}")
        f(f"{'' if err is None else str(err)}")

    return None


def make_custom_substitutions(path, custom_env, default=None, skip_escaped=False):
    def replace_var(m, custom_env):
        return custom_env.get(m.group(2) or m.group(1), m.group(0) if default is None else default)
    reVar = (r'(?<!\\)' if skip_escaped else '') + r'\$(\w+|\{([^}]*)\})'

    f = lambda x: replace_var(x, custom_env)

    return re.sub(reVar, f, path)


def relative_to_absolute(path_string, custom_env=None):
    return string_to_path(path_string, custom_env)


def relatives_to_absolutes(paths, custom_env=None):
    f = lambda x: string_to_path(x, custom_env)
    return list(map(f, paths))


def create_dir_structure(file_path, custom_env=None):
    path = string_to_path(file_path)
    directory_path = path.parents[0]

    if not directory_path.is_dir():
        directory_path.mkdir(parents=True, exist_ok=True)

    return None

def string_to_path(path_string, custom_env=None):
    if custom_env is not None:
        path_string = make_custom_substitutions(path_string, custom_env)    

    return Path(os.path.expandvars(path_string)).resolve()

def is_file(path_string, custom_env=None):
    return string_to_path(path_string, custom_env).is_file()