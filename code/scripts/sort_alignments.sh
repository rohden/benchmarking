genomes=()
POSITIONAL=()

while [[ $# -gt 0 ]]
do
    key="$1"

    case $key in
        -h|--help)
        printf "
Usage: ${0} [OPTIONS]
General Options:
  [-h | --help]               display this help menu

Input File Options:
  [-ec | --ecoli]             use the ecoli genome
                                this asserts a folder named \"genomes/altered/ecoli\aligned\"
                                exists in the benchmarking directory
  [-dv | --dvulgaris]         use the dvulgaris genome
                                this asserts a folder named \"genomes/altered/dvulgaris\aligned\"
                                exists in the benchmarking directory
  [-mm | --mmaripuladis]      use the mmaripuladis genome 
                                this asserts a folder named \"genomes/altered/mmaripuladis\aligned\"
                                exists in the benchmarking directory
  [-ag | --additional-genome] use the provided genome 
                                this asserts a folder named \"genomes/altered/{arg}\aligned\"
                                exists in the benchmarking directory
                                this option may be provided multiple times to use multiple folders
"
        exit 0
        ;;
        # turn on verbose execution
        -v|--verbose)
        VERBOSE=true
        shift # past argument
        ;;
        # whether or not to use e. coli genome
        -ec|--ecoli)
        genomes+=("ecoli")
        shift # past argument
        ;;
        # whether or not to use d. vulgaris genome
        -dv|--dvulgaris)
        genomes+=("dvulgaris")
        shift # past argument
        ;;
        # whether or not to use m. maripaludis genome
        -mm|--mmaripuladis)
        genomes+=("mmaripuladis")
        shift # past argument
        ;;
        # additional genomes in user-specified paths
        -ag|--additional-genome)
        genomes+=("$2")
        shift # past argument
        shift # past value
        ;;
        *)    # unknown option
        POSITIONAL+=("$1") # save it in an array for later
        shift # past argument
        ;;
    esac
done
set -- "${POSITIONAL[@]}" # restore positional parameters


for genome in "${genomes[@]}"; do
    pushd ${BENCHMARKING_HOME}/genomes/altered/${genome}
    for cur_dir in */ ; do 
        for coverage in 20 30 40 50 ; do 
            printf "working on ${cur_dir} @ ${coverage} for ${genome}\n"

            # generate the alignments
            printf "compressing bwa alignment ...\n"
            samtools view -S -b ${cur_dir}aligned/${coverage}x/bwa/reads_aligned.sam > ${cur_dir}aligned/${coverage}x/bwa/reads_aligned.bam
            printf "samtools exited with code ${?}\n"

            printf "sorting bwa alignment ...\n"
            samtools sort ${cur_dir}aligned/${coverage}x/bwa/reads_aligned.bam -o ${cur_dir}aligned/${coverage}x/bwa/reads_aligned.sorted.bam
            printf "samtools exited with code ${?}\n"
        
            printf "compressing bowtie2 alignment ...\n"
            samtools view -S -b ${cur_dir}aligned/${coverage}x/bowtie2/reads_aligned.sam > ${cur_dir}aligned/${coverage}x/bowtie2/reads_aligned.bam
            printf "samtools exited with code ${?}\n"
            
            printf "sorting bowtie2 alignment ...\n"
            samtools sort ${cur_dir}aligned/${coverage}x/bowtie2/reads_aligned.bam -o ${cur_dir}aligned/${coverage}x/bowtie2/reads_aligned.sorted.bam
            printf "samtools exited with code ${?}\n"

            printf "done with ${cur_dir} @ ${coverage} for ${genome}\n"
        done

        printf "done with ${cur_dir} for ${genome}\n"
    done

    printf "done with ${genome}\n"
    popd
done