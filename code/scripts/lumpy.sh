#!/bin/bash

lumpyexpress_bin=${LUMPY_BIN}

working_dir="./temp"
config_path="${BENCHMARKING_HOME}/code/configs/lumpyexpress.config"
clean_temp=true

POSITIONAL=()
while [[ $# -gt 0 ]]
do
    key="$1"

    case $key in
        # path to alignment file in bam format
        -a|--alignment)
        alignment_file="$2"
        shift # past argument
        shift # past value
        ;;
        # path to splitters file in bam format
        -s|--splitters)
        splitters_file="$2"
        shift # past argument
        shift # past value
        ;;
        # path to splitters file in bam format
        -d|--discordants)
        discordants_file="$2"
        shift # past argument
        shift # past value
        ;;
        # path to reference genome file in fasta format
        -ref|--reference-genome)
        reference_file="$2"
        shift # past argument
        shift # past value
        ;;
        # output filename
        -o|--output)
        output_file="$2"
        shift # past argument
        shift # past value
        ;;
        # path to lumpyexpress config file
        -c|--config)
        config_path="$2"
        shift # past argument
        shift # past value
        ;;
        # working directory where temporary data is stored
        # defaults to ./temp
        --working-dir)
        working_dir="$2"
        shift # past argument
        shift # past value
        ;;
        # elect to not delete temporary files
        --keep-temp)
        clean_temp=false
        shift # past argument
        ;;
        *)    # unknown option
        POSITIONAL+=("$1") # save it in an array for later
        shift # past argument
        ;;
    esac
done
set -- "${POSITIONAL[@]}" # restore positional parameters


aligned_reads="aligned_reads.bam"
ref="reference.fa"
splitters="splitters.bam"
discordants="discordants.bam"

mkdir -p ${working_dir}
cp ${alignment_file} ${working_dir}/${aligned_reads}
cp ${splitters_file} ${working_dir}/${splitters}
cp ${discordants_file} ${working_dir}/${discordants}
cp ${reference_file} ${working_dir}/${ref}

pushd ${working_dir}

time ${lumpyexpress_bin} -K ${config_path} -B ${aligned_reads} -S ${splitters} -D ${discordants} -o ${output_file}

popd

if [[ ${clean_temp} == true ]] ; then 
    rm -rf ${working_dir}
fi
