#!/bin/bash

log_message() {
    printf "`date` -- ${@} \n"
}

genomes=()
POSITIONAL=()

while [[ $# -gt 0 ]]
do
    key="$1"

    case $key in
        -h|--help)
        printf "
Usage: ${0} [OPTIONS]
General Options:
  [-h | --help]               display this help menu

Input File Options:
  [-ec | --ecoli]             use the ecoli genome
  [-dv | --dvulgaris]         use the dvulgaris genome
  [-mm | --mmaripuladis]      use the mmaripuladis genome 
  [-ag | --additional-genome] use the provided genome 
                                this option may be provided multiple times to use multiple folders
"
        exit 0
        ;;
        # turn on verbose execution
        -v|--verbose)
        VERBOSE=true
        shift # past argument
        ;;
        # whether or not to use e. coli genome
        -ec|--ecoli)
        genomes+=("ecoli")
        shift # past argument
        ;;
        # whether or not to use d. vulgaris genome
        -dv|--dvulgaris)
        genomes+=("dvulgaris")
        shift # past argument
        ;;
        # whether or not to use m. maripaludis genome
        -mm|--mmaripuladis)
        genomes+=("mmaripuladis")
        shift # past argument
        ;;
        # additional genomes in user-specified paths
        -ag|--additional-genome)
        genomes+=("$2")
        shift # past argument
        shift # past value
        ;;
        *)    # unknown option
        POSITIONAL+=("$1") # save it in an array for later
        shift # past argument
        ;;
    esac
done
set -- "${POSITIONAL[@]}" # restore positional parameters

gustaf="${BENCHMARKING_HOME}/code/scripts/gustaf.sh"

# these should really be parameters/config file
sets=("all" "deletions" "duplications" "insertions" "inversions" "translocations")
coverages=(20 30 40 50)
log_file="${BENCHMARKING_HOME}/logs/gustaf.log"

mkdir "${BENCHMARKING_HOME}/logs"

for genome in "${genomes[@]}"; do
    log_message "creating output directories for ${genome}"
    mkdir -p "data/${genome}/gustaf" 

    log_message "working on ${genome} ... "
    for data_set in "${sets[@]}" ; do

	if [[ $genome = "dvulgaris" && ! ( $data_set = "all" || ${data_set} = "deletions" ) ]] ; then
	       log_message "skipping $genome -- $data_set @ $coverage due to repeat"
	       continue
        fi
        if [[ -d "${PWD}/genomes/altered/${genome}/${data_set}" ]] ; then
            log_message "working on ${data_set} for ${genome}"

            # go over all the different coverages we expect to have for this data set
            for coverage in "${coverages[@]}" ; do
                if [[ $genome = "dvulgaris" && $data_set = "deletions" && $coverage > 30 ]] ; then
			        log_message "skipping ${coverage}x for ${data_set} for ${genome} due to repeat"
			        continue
	            fi 

	    	    log_message "working on ${coverage}x for ${data_set} for ${genome}"

                # general paths
                reads1_path="${PWD}/genomes/altered/${genome}/${data_set}/reads/${coverage}x/reads_paired1.fixed.fq"
                reads2_path="${PWD}/genomes/altered/${genome}/${data_set}/reads/${coverage}x/reads_paired2.fixed.fq"
                reference_path="${PWD}/genomes/reference/${genome}/speedseq/reference.normalized.fa"

                # gustaf specific paths
                work_dir="${PWD}/data/${genome}/gustaf/${data_set}_${coverage}x_temp"
                out_prefix="${PWD}/data/${genome}/gustaf/${data_set}_${coverage}x"

                log_message "running gustaf for ${genome} - ${data_set} @ ${coverage}x"
                time /bin/bash ${gustaf} -r1 ${reads1_path} -r2 ${reads2_path} -ref ${reference_path} \
                                    -o ${out_prefix} --working-dir ${work_dir} >> ${log_file} 2>&1
                log_message "completed gustaf for ${genome} - ${data_set} @ ${coverage}x"

            done
        fi
    done
done

