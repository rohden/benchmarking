#!/bin/bash

log_message() {
    printf "`date` -- ${@} \n"
}

genomes=()
POSITIONAL=()

while [[ $# -gt 0 ]]
do
    key="$1"

    case $key in
        -h|--help)
        printf "
Usage: ${0} [OPTIONS]
General Options:
  [-h | --help]               display this help menu

Input File Options:
  [-ec | --ecoli]             use the ecoli genome
  [-dv | --dvulgaris]         use the dvulgaris genome
  [-mm | --mmaripuladis]      use the mmaripuladis genome 
  [-ag | --additional-genome] use the provided genome 
                                this option may be provided multiple times to use multiple folders
"
        exit 0
        ;;
        # turn on verbose execution
        -v|--verbose)
        VERBOSE=true
        shift # past argument
        ;;
        # whether or not to use e. coli genome
        -ec|--ecoli)
        genomes+=("ecoli")
        shift # past argument
        ;;
        # whether or not to use d. vulgaris genome
        -dv|--dvulgaris)
        genomes+=("dvulgaris")
        shift # past argument
        ;;
        # whether or not to use m. maripaludis genome
        -mm|--mmaripuladis)
        genomes+=("mmaripuladis")
        shift # past argument
        ;;
        # additional genomes in user-specified paths
        -ag|--additional-genome)
        genomes+=("$2")
        shift # past argument
        shift # past value
        ;;
        *)    # unknown option
        POSITIONAL+=("$1") # save it in an array for later
        shift # past argument
        ;;
    esac
done
set -- "${POSITIONAL[@]}" # restore positional parameters

svd="${BENCHMARKING_HOME}/code/scripts/svdetect.sh"

# these should really be parameters/config file
sets=("all" "deletions" "duplications" "insertions" "inversions" "translocations")
coverages=(20 30 40 50)
log_file="${BENCHMARKING_HOME}/logs/run_svd.log"

mkdir "${BENCHMARKING_HOME}/logs"

for genome in "${genomes[@]}"; do
    log_message "creating output directory for ${genome}"
    mkdir -p "${BENCHMARKING_HOME}/data/${genome}/svdetect"

    log_message "working on ${genome} ... "
    for data_set in "${sets[@]}" ; do

        # only run if the dataset actually exists
        if [[ -d "${BENCHMARKING_HOME}/genomes/altered/${genome}/${data_set}" ]] ; then
            log_message "working on ${data_set} for ${genome}"

            # go over all the different coverages we expect to have for this data set
            for coverage in "${coverages[@]}" ; do
                log_message "working on ${coverage}x for ${data_set} for ${genome}"

                # file paths
                reference_path="${BENCHMARKING_HOME}/genomes/reference/${genome}/bwa/reference.normalized.fa"
                cmap_path="${BENCHMARKING_HOME}/genomes/reference/${genome}/bwa/chromosome_lengths.len"
                alignment_path="${BENCHMARKING_HOME}/genomes/altered/${genome}/${data_set}/aligned/${coverage}x/bwa/reads_aligned.sam"
                work_dir="${BENCHMARKING_HOME}/data/${genome}/svdetect/${data_set}_${coverage}x_temp"
                out_file="${BENCHMARKING_HOME}/data/${genome}/svdetect/${data_set}_${coverage}x.vcf"

                log_message "running SVDetect for ${genome} - ${data_set} @ ${coverage}x "
                time /bin/bash ${svd} -a ${alignment_path} -r ${reference_path} -c ${cmap_path} \
                                 --working-dir ${work_dir} -o ${out_file} -t $(nproc) >> ${log_file}
                
            done
        fi
    done
done

