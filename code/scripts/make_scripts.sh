#!/bin/bash

conf_dir=${BENCHMARKING_HOME}/genomes/variants/${2}
script_bin=${BENCHMARKING_HOME}/bin/dist/simulate_sv

${script_bin} --conf ${conf_dir}/all/all_1.toml $1
${script_bin} --conf ${conf_dir}/all/all_2.toml $1

${script_bin} --conf ${conf_dir}/deletions/deletions_1.toml $1
${script_bin} --conf ${conf_dir}/deletions/deletions_2.toml $1

${script_bin} --conf ${conf_dir}/insertions/insertions_1.toml $1
${script_bin} --conf ${conf_dir}/insertions/insertions_2.toml $1

${script_bin} --conf ${conf_dir}/inversions/inversions_1.toml $1
${script_bin} --conf ${conf_dir}/inversions/inversions_2.toml $1

${script_bin} --conf ${conf_dir}/duplications/duplications_1.toml $1
${script_bin} --conf ${conf_dir}/duplications/duplications_2.toml $1
