#!/bin/bash

# basic setup
seed=12345678
references_dir="${BENCHMARKING_HOME}/genomes/reference"
art_binary=${ART_BIN}

SORT_ALN=true
COMPRESS_ALN=true

genomes=()
POSITIONAL=()
while [[ $# -gt 0 ]]
do
    key="$1"

    case $key in
        -h|--help)
        printf "
Usage: ${0} [OPTIONS]

General Options:
  [-v | --verbose]            turn ON verbose output
  [-h | --help]               display this help menu
  [-s | --seed]               random seed to use for ART

Reference Genome Options:
  [--index-bwa-ref]           turn ON generating index for bwa reference
  [--index-bowtie2-ref]       turn ON generating index for bowtie2 reference
  [--index-speedseq-ref]      turn ON generating index for speedseq reference

Alignment Tools Options:
  [-bwa | --bwa]              turn ON generating alignment for bwa
  [-bt2 | --bowtie2]          turn ON generating alignment for bowtie2
  [-ss | --speedseq]          turn ON generating alignment for speedseq

Alignment Options:
  [-gr | --generate-reads]    turn ON generating reads for the given reference
                              this must be ON if reads were not generated manually
  [-ns | --no-sort]           turn OFF sorting the alignments for bwa and bowtie2
  [-nc | --no-compress]       turn OFF compressing the alignments for bwa and bowtie2

Input File Options:
  [-ec | --ecoli]             use the ecoli genome
                                this asserts a folder named \"genomes/altered/ecoli\"
                                exists in the benchmarking directory
  [-dv | --dvulgaris]         use the dvulgaris genome
                                this asserts a folder named \"genomes/altered/dvulgaris\"
                                exists in the benchmarking directory
  [-mm | --mmaripuladis]      use the mmaripuladis genome 
                                this asserts a folder named \"genomes/altered/mmaripuladis\"
                                exists in the benchmarking directory
  [-ag | --additional-genome] use the provided genome 
                                this asserts a folder named \"genomes/altered/{arg}\"
                                exists in the benchmarking directory
                                this option may be provided multiple times to use multiple folders
"
        exit 0
        ;;
        # turn on generating reference index for bwa
        -s|--random-seed)
        seed=$2
        shift # past argument
        shift # past value
        ;;
        # turn on generating reference index for bwa
        --index-bwa-ref)
        BWA=true
        shift # past argument
        ;;
        # turn on generating reference index for bowtie2
        --index-bowtie2-ref)
        BOWTIE=true
        shift # past argument
        ;;
        # turn on generating reference index for speedseq
        --index-speedseq-ref)
        SPEEDSEQ=true
        shift # past argument
        ;;
        # align with bwa
        -bwa|--bwa)
        USE_BWA=true
        shift # past argument
        ;;
        # align with bowtie2
        -bt2|--bowtie2)
        USE_BOWTIE=true
        shift # past argument
        ;;
        # align with speedseq
        -ss|--speedseq)
        USE_SPEEDSEQ=true
        shift # past argument
        ;;
        # turn on generating reads for selected genomes
        -gr|--generate-reads)
        READS=true
        shift # past argument
        ;;
        # turn of compressing the alignments
        -nc|--no-compress)
        COMPRESS_ALN=false
        SORT_ALN=false
        shift # past argument
        ;;
        # turn off sorting the alignments
        -ns|--no-sort)
        SORT_ALN=false
        shift # past argument
        ;;
        # turn on verbose execution
        -v|--verbose)
        VERBOSE=true
        shift # past argument
        ;;
        # whether or not to use e. coli genome
        -ec|--ecoli)
        use_ec=true
        shift # past argument
        ;;
        # whether or not to use d. vulgaris genome
        -dv|--dvulgaris)
        use_dv=true
        shift # past argument
        ;;
        # whether or not to use m. maripaludis genome
        -mm|--mmaripuladis)
        use_mm=true
        shift # past argument
        ;;
        # additional genomes in user-specified paths
        -ag|--additional-genome)
        genomes+=("$2")
        shift # past argument
        shift # past value
        ;;
        *)    # unknown option
        POSITIONAL+=("$1") # save it in an array for later
        shift # past argument
        ;;
    esac
done
set -- "${POSITIONAL[@]}" # restore positional parameters

if [[ ${use_ec} == true ]] ; then
    genomes+=("ecoli")
fi
if [[ ${use_dv} == true ]] ; then
    genomes+=("dvulgaris")
fi
if [[ ${use_mm} == true ]] ; then
    genomes+=("mmaripuladis")
fi

# setup reference for bwa if needed
if [[ ${BWA} = true ]] ; then
    if [[ ${VERBOSE} == true ]] ; then
        printf "Generating index for bwa reference\n"
    fi

    for genome in "${genomes[@]}"; do
        python3.6 ${BENCHMARKING_HOME}/code/normalization/normalize_reference.py --ref ${references_dir}/${genome}/bwa/reference.fa
        pushd ${references_dir}/${genome}/bwa
        bwa index reference.normalized.fa > bwa_index_out.txt 2>&1
        samtools faidx reference.normalized.fa
        cut -f1,2 reference.normalized.fa.fai > chromosome_lengths.len
        popd
    done
fi

# setup reference for bowtie2 if needed
if [[ ${BOWTIE} = true ]] ; then
    if [[ ${VERBOSE} == true ]] ; then
        printf "Generating index for bowtie2 reference\n"
    fi

    for genome in "${genomes[@]}"; do
        python3.6 ${BENCHMARKING_HOME}/code/normalization/normalize_reference.py --ref ${references_dir}/${genome}/bowtie2/reference.fa
        pushd ${references_dir}/${genome}/bowtie2
        bowtie2-build reference.normalized.fa reference_index.out > bt2_index_out.txt 2>&1
        samtools faidx reference.normalized.fa
        cut -f1,2 reference.normalized.fa.fai > chromosome_lengths.len
        popd
    done
fi

# setup reference for speedseq if needed
if [[ ${SPEEDSEQ} = true ]] ; then
    if [[ ${VERBOSE} == true ]] ; then
        printf "Generating index for speedseq reference\n"
    fi

    for genome in "${genomes[@]}"; do
        python3.6 ${BENCHMARKING_HOME}/code/normalization/normalize_reference.py --ref ${references_dir}/${genome}/speedseq/reference.fa
        pushd ${references_dir}/${genome}/speedseq
        samtools faidx reference.normalized.fa
        cut -f1,2 reference.normalized.fa.fai > chromosome_lengths.len
        popd
    done
fi

for genome in "${genomes[@]}"; do
    if [[ ${VERBOSE} == true ]] ; then
        printf "Working on ${genome} ... \n"
    fi

    pushd ${BENCHMARKING_HOME}/genomes/altered/${genome}

    for cur_dir in */ ; do 
        if [[ ${VERBOSE} == true ]] ; then
            printf "Working on ${cur_dir} for ${genome}\n"
        fi

        for coverage in 20 30 40 50 ; do 
            if [[ ${VERBOSE} == true ]] ; then
                printf "Working on ${coverage} in ${cur_dir} for ${genome}\n"
            fi

            # make reads if needed
            if [[ ${READS} == true ]] ; then
                mkdir -p ${cur_dir}reads/${coverage}x || true

                if [[ ${VERBOSE} == true ]] ; then
                    printf "Generating reads for ${coverage} in ${cur_dir} for ${genome}\n"
                fi

                ${art_binary} -rs ${seed} -ss HS25 -i ${cur_dir}genome_rearranged.fasta -p -l 100 -f ${coverage} -m 200 -s 10 -na -o ${cur_dir}reads/${coverage}x/reads_paired -rs ${seed} > ${cur_dir}reads/${coverage}x/output.txt 2>&1
                code=$?

                if [[ ${VERBOSE} == true || ${code} != 0 ]] ; then
                    printf "Art exited with status = ${code} for ${coverage} in ${cur_dir} for ${genome}\n"
                fi

                python3.6 ${BENCHMARKING_HOME}/code/normalization/normalize_reads.py --prefix ${cur_dir}reads/${coverage}x/reads_paired
                code=$?
                if [[ ${VERBOSE} == true || ${code} != 0 ]] ; then
                    printf "Python ART read normalizer exited with status = ${code} for ${coverage} in ${cur_dir} for ${genome}\n"
                fi
            fi

            if [[ ${USE_BWA} == true ]] ; then 
                mkdir -p ${cur_dir}aligned/${coverage}x/bwa || true

                if [[ ${VERBOSE} == true ]] ; then
                    printf "Generating bwa alignment for ${coverage} in ${cur_dir} for ${genome}\n"
                fi

                bwa mem -t $(nproc) ${references_dir}/${genome}/bwa/reference.normalized.fa ${cur_dir}reads/${coverage}x/reads_paired1.fixed.fq ${cur_dir}reads/${coverage}x/reads_paired2.fixed.fq > ${cur_dir}aligned/${coverage}x/bwa/reads_aligned.sam 2>${cur_dir}aligned/${coverage}x/bwa/output.txt
                code=$?

                if [[ ${VERBOSE} == true || ${code} != 0 ]] ; then
                    printf "bwa exited with status = ${code} for ${coverage} in ${cur_dir} for ${genome}\n"
                fi

                if [[ ${SORT_ALN} == true ]] ; then
                    samtools view -S -b ${cur_dir}aligned/${coverage}x/bwa/reads_aligned.sam > ${cur_dir}aligned/${coverage}x/bwa/reads_aligned.bam
                    code=$?
                    if [[ ${VERBOSE} == true || ${code} != 0 ]] ; then
                        printf "samtools exited with status = ${code} after compressing bwa alignment for ${coverage} in ${cur_dir} for ${genome}\n"
                    fi
                fi
                
                if [[ ${COMPRESS_ALN} == true ]] ; then
                    samtools sort ${cur_dir}aligned/${coverage}x/bwa/reads_aligned.bam -o ${cur_dir}aligned/${coverage}x/bwa/reads_aligned.sorted.bam
                    code=$?
                    if [[ ${VERBOSE} == true || ${code} != 0 ]] ; then
                        printf "samtools exited with status = ${code} after sorting bwa alignment for ${coverage} in ${cur_dir} for ${genome}\n"
                    fi
                fi

                if [[ ${VERBOSE} == true ]] ; then
                    printf "Done generating bwa alignment for ${coverage} in ${cur_dir} for ${genome}\n"
                fi
            fi

            if [[ ${USE_BOWTIE} == true ]] ; then 
                mkdir -p ${cur_dir}aligned/${coverage}x/bowtie2 || true

                if [[ ${VERBOSE} == true ]] ; then
                    printf "Generating bowtie2 alignment for ${coverage} in ${cur_dir} for ${genome}\n"
                fi

                bowtie2 -p 48 -X 5000 --rf -x ${references_dir}/${genome}/bowtie2/reference_index.out -1 ${cur_dir}reads/${coverage}x/reads_paired1.fixed.fq -2 ${cur_dir}reads/${coverage}x/reads_paired2.fixed.fq -S ${cur_dir}aligned/${coverage}x/bowtie2/reads_aligned.sam > ${cur_dir}aligned/${coverage}x/bowtie2/output.txt 2>&1
                code=$?

                if [[ ${VERBOSE} == true || ${code} != 0 ]] ; then
                    printf "bowtie2 exited with status = ${code} for ${coverage} in ${cur_dir} for ${genome}\n"
                fi

                if [[ ${SORT_ALN} == true ]] ; then
                    samtools view -S -b ${cur_dir}aligned/${coverage}x/bowtie2/reads_aligned.sam > ${cur_dir}aligned/${coverage}x/bowtie2/reads_aligned.bam
                    code=$?
                    if [[ ${VERBOSE} == true || ${code} != 0 ]] ; then
                        printf "samtools exited with status = ${code} after compressing bowtie2 alignment for ${coverage} in ${cur_dir} for ${genome}\n"
                    fi
                fi

                if [[ ${COMPRESS_ALN} == true ]] ; then
                    samtools sort ${cur_dir}aligned/${coverage}x/bowtie2/reads_aligned.bam -o ${cur_dir}aligned/${coverage}x/bowtie2/reads_aligned.sorted.bam
                    code=$?
                    if [[ ${VERBOSE} == true || ${code} != 0 ]] ; then
                        printf "samtools exited with status = ${code} after sorting bowtie2 alignment for ${coverage} in ${cur_dir} for ${genome}\n"
                    fi
                fi

                if [[ ${VERBOSE} == true ]] ; then
                    printf "Done generating bowtie2 alignment for ${coverage} in ${cur_dir} for ${genome}\n"
                fi
            fi

            if [[ ${USE_SPEEDSEQ} == true ]] ; then 
                mkdir -p ${cur_dir}aligned/${coverage}x/speedseq || true

                if [[ ${VERBOSE} == true ]] ; then
                    printf "Generating speedseq alignment for ${coverage} in ${cur_dir} for ${genome}\n"
                fi

                # speedseq compresses and sorts aliognments automatically
                speedseq align -R "@RG\tID:id\tSM:sample\tLB:lib" -o ${cur_dir}aligned/${coverage}x/speedseq/reads_aligned -t $(nproc) ${references_dir}/${genome}/speedseq/reference.normalized.fa ${cur_dir}reads/${coverage}x/reads_paired1.fixed.fq ${cur_dir}reads/${coverage}x/reads_paired2.fixed.fq > ${cur_dir}aligned/${coverage}x/speedseq/output.txt 2>&1
                code=$?

                if [[ ${VERBOSE} == true || ${code} != 0 ]] ; then
                    printf "speedseq exited with status = ${code} for ${coverage} in ${cur_dir} for ${genome}\n"
                fi

                if [[ ${code} == 0 ]] ; then 
                    mv reads_paired1.fixed.fq.* ${cur_dir}aligned/${coverage}x/speedseq
                fi

                if [[ ${VERBOSE} == true ]] ; then
                    printf "Done generating speedseq alignment for ${coverage} in ${cur_dir} for ${genome}\n"
                fi

            fi

            if [[ ${VERBOSE} == true ]] ; then
                printf "Done with ${coverage} in ${cur_dir} for ${genome}\n"
            fi
        done

        if [[ ${VERBOSE} == true ]] ; then
            printf "Done with ${cur_dir} for ${genome}\n"
        fi
    done

    if [[ ${VERBOSE} == true ]] ; then
            printf "Done with ${genome}\n"
    fi

    popd
done

if [[ ${VERBOSE} == true ]] ; then
    printf "Done \n"
fi
