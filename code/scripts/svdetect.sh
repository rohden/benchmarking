#!/bin/bash

svd_bin=${SVD_BIN}
preprocess_script=${SVD_PP}

working_dir="./temp"
clean_temp=true
nthreads=`nproc`
config_file=""
output_dir="./"

POSITIONAL=()
while [[ $# -gt 0 ]]
do
    key="$1"

    case $key in
        # absolute path to alignment file in sam format
        -a|--alignment)
        alignment_file="$2"
        shift # past argument
        shift # past value
        ;;
        # absolute path to reference genome in fasta format
        -r|--reference)
        reference_file="$2"
        shift # past argument
        shift # past value
        ;;
        # absolute path to chromosome lengths file 
        # format must be as follows:
        # {number}<tab>{name}<tab>{length}
        # numbers must begin at 1 and be sequential integers
        # no spaces between the three entries are allowed
        # one entry per line
        # Note-to-self: current way of making this file 
        # does not create it correctly, it ommits the first column
        # we need fix that sometime ... bowtie2 seems to do it right
        # maybe switch to that instead of that weird abomination script?
        -c|--chromosome-map)
        cmap_file="$2"
        shift # past argument
        shift # past value
        ;;
        # absolute path to config file to use for svdetect
        # defaults creates a new file in working directory
        # with a default configuration 
        # if this argument is included, the user is responsible
        # for ensuring the correct file names are included
        # as reference, alignment and cmap are renamed on lines 83-88
        --config)
        config_file="$2"
        shift # past argument
        shift # past value
        ;;
        # number of threads to use
        -t|--nthreads)
        nthreads="$2"
        shift # past argument
        shift # past value
        ;;
        # absolute path to output file
        -o|--output)
        output_file="$2"
        shift # past argument
        shift # past value
        ;;
        # working directory where temporary data is stored
        # defaults to ./temp
        # this directory will be DELETED by default
        --working-dir)
        working_dir="$2"
        shift # past argument
        shift # past value
        ;;
        # elect to keep temporary files
        --keep-temp)
        clean_temp=false
        shift # past argument
        ;;
        *)    # unknown option
        POSITIONAL+=("$1") # save it in an array for later
        shift # past argument
        ;;
    esac
done
set -- "${POSITIONAL[@]}" # restore positional parameters

aligned_reads_prefix="aligned_reads"
aligned_reads="${aligned_reads_prefix}.sam"
aligned_reads_processed="${aligned_reads_prefix}.ab.sam"
out_file="${aligned_reads_processed}.links.filtered.sv.txt"
reference="reference.fa"
cmap="reference.len"
config="svdetect.conf"
preprocess_out="temp.txt"

mkdir -p ${working_dir}
cp ${alignment_file} ${working_dir}/${aligned_reads_prefix}.sam
cp ${reference_file} ${working_dir}/${reference}
cp ${cmap_file} ${working_dir}/${cmap}

# preprocessing output must go to file to get the  mu length and sigma length values 
time perl ${preprocess_script} -o ${working_dir} ${working_dir}/${aligned_reads} > ${working_dir}/${preprocess_out} 2>&1

# grab the mu length value and sigma length value from the output file to use in the next step
mu_length=`cat ${working_dir}/${preprocess_out} | grep "mu length = " | awk '{print $5 $9}' | awk -F',' '{print $1}'`
sigma_length=`cat ${working_dir}/${preprocess_out} | grep "mu length = " | awk '{print $5 $9}' | awk -F',' '{print $2}'`

# only produce a config file if the user didn't provide one
if [[ ! -f ${config_file} ]] ; then 
    cat <<EOF >> ${working_dir}/${config}
<general>
	input_format=sam
	sv_type=all
	mates_orientation=FR
	read1_length=100
	read2_length=100
	mates_file=${working_dir}/${aligned_reads_processed}
	cmap_file=${working_dir}/${cmap}
	num_threads=8
	output_dir=${working_dir}
</general>
<detection>
    split_link_file=1
    window_size=6541
    step_length=1635
</detection>
<filtering>
    strand_filtering=1
    order_filtering=1
    insert_size_filtering=1
    nb_pairs_threshold=2
    nb_pairs_order_threshold=2
    indel_sigma_threshold=3
    dup_sigma_threshold=2
    final_score_threshold=0.8
    mu_length=${mu_length}
    sigma_length=${sigma_length}
</filtering>
EOF
else 
    cp ${config_file} ${working_dir}/${config}
fi

pushd ${working_dir}

time ${svd_bin} linking filtering links2SV -conf ${config}

cp ${out_file} ${output_file}

popd

if [[ ${clean_temp} == true ]] ; then 
    rm -rf ${working_dir}
fi
