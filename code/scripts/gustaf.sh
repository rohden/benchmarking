#!/bin/bash

gustaf_prep_bin="${GUSTAF_MJ}"
gustaf_bin="${GUSTAF_BIN}"
stellar_bin="${STELLAR_BIN}"

n_threads=1
working_dir="./temp"
clean_temp=true

POSITIONAL=()
while [[ $# -gt 0 ]]
do
    key="$1"

    case $key in
        # path to first reads file in fastq format
        # if sam is provided, -sam must also be set
        -r1|--reads1)
        reads1_file="$2"
        shift # past argument
        shift # past value
        ;;
        # path to second reads file in fastq format
        # if sam is provided, -sam must also be set
        -r2|--reads2)
        reads2_file="$2"
        shift # past argument
        shift # past value
        ;;
        # path to reference file in fasta format
        # if sam is provided, -sam must also be set
        -ref|--reference-genome)
        ref_file="$2"
        shift # past argument
        shift # past value
        ;;
        # output file prefix
        -o|--output)
        out_prefix="$2"
        shift # past argument
        shift # past value
        ;;
        # output file-prefix
        -t|--num-threads)
        n_threads="$2"
        shift # past argument
        shift # past value
        ;;
        # working directory where temporary data is stored
        # defaults to ./temp
        --working-dir)
        working_dir="$2"
        shift # past argument
        shift # past value
        ;;
        # elect to not delete temporary files
        --keep-temp)
        clean_temp=false
        shift # past argument
        ;;
        *)    # unknown option
        POSITIONAL+=("$1") # save it in an array for later
        shift # past argument
        ;;
    esac
done
set -- "${POSITIONAL[@]}" # restore positional parameters

r1_name=$(basename -- "$reads1_file")
r2_name=$(basename -- "$reads2_file")
ref_name=$(basename -- "$ref_file")
extension1="${r1_name##*.}"
extension2="${r2_name##*.}"
extension3="${ref_name##*.}"

reads1="reads1"
reads2="reads2"
combined_reads="joined_reads.fa"
stellar_aligned="joined_reads.gff"
reference="reference"

mkdir -p ${working_dir}
echo "cp ${reads1_file} ${working_dir}/${reads1}.${extension1}"
cp ${reads1_file} ${working_dir}/${reads1}.${extension1}
echo "cp ${reads2_file} ${working_dir}/${reads2}.${extension2}"
cp ${reads2_file} ${working_dir}/${reads2}.${extension2}
echo "cp ${ref_file} ${working_dir}/${reference}.${extension3}"
cp ${ref_file} ${working_dir}/${reference}.${extension3}

pushd ${working_dir}

echo "joining mates ..."
time gustaf_prep_bin -rc -o ${combined_reads} ${reads1}.${extension1} ${reads2}.${extension2}

echo "running stellar ..."
time stellar_bin -l 30 -o ${stellar_aligned} ${reference}.${extension3} ${combined_reads}

gustaf_options="-m ${stellar_aligned} -nth ${n_threads} -ll 1000 -le 30 -rc -gff ${out_prefix}.gff -vcf ${out_prefix}.vcf"
echo "running gustaf ..."

time gustaf_bin ${gustaf_options} ${reference}.${extension3} ${reads1}.${extension1} ${reads2}.${extension2}

echo "done!"

popd

if [[ ${clean_temp} == true ]] ; then 
    rm -rf ${working_dir}
fi
