#!/bin/bash

log_message() {
    printf "`date` -- ${@} \n"
}

genomes=()
POSITIONAL=()

while [[ $# -gt 0 ]]
do
    key="$1"

    case $key in
        -h|--help)
        printf "
Usage: ${0} [OPTIONS]
General Options:
  [-h | --help]               display this help menu

Input File Options:
  [-ec | --ecoli]             use the ecoli genome
  [-dv | --dvulgaris]         use the dvulgaris genome
  [-mm | --mmaripuladis]      use the mmaripuladis genome 
  [-ag | --additional-genome] use the provided genome 
                                this option may be provided multiple times to use multiple folders
"
        exit 0
        ;;
        # turn on verbose execution
        -v|--verbose)
        VERBOSE=true
        shift # past argument
        ;;
        # whether or not to use e. coli genome
        -ec|--ecoli)
        genomes+=("ecoli")
        shift # past argument
        ;;
        # whether or not to use d. vulgaris genome
        -dv|--dvulgaris)
        genomes+=("dvulgaris")
        shift # past argument
        ;;
        # whether or not to use m. maripaludis genome
        -mm|--mmaripuladis)
        genomes+=("mmaripuladis")
        shift # past argument
        ;;
        # additional genomes in user-specified paths
        -ag|--additional-genome)
        genomes+=("$2")
        shift # past argument
        shift # past value
        ;;
        *)    # unknown option
        POSITIONAL+=("$1") # save it in an array for later
        shift # past argument
        ;;
    esac
done
set -- "${POSITIONAL[@]}" # restore positional parameters

bdm="${BENCHMARKING_HOME}/code/scripts/breakdancer.sh"
gustaf="${BENCHMARKING_HOME}/code/scripts/gustaf.sh"
lumpy="${BENCHMARKING_HOME}/code/scripts/lumpy.sh"
cnvnator="${BENCHMARKING_HOME}/code/scripts/cnvnator.sh"
svdetect="${BENCHMARKING_HOME}/code/scripts/svdetect.sh"

# these should really be parameters/config file
sets=("all" "deletions" "duplications" "insertions" "inversions" "translocations")
coverages=(20 30 40 50)
log_file="${BENCHMARKING_HOME}/logs/run_all.log"

mkdir "${BENCHMARKING_HOME}/logs"

for genome in "${genomes[@]}"; do
    log_message "creating output directories for ${genome}"
    mkdir -p "${BENCHMARKING_HOME}/data/${genome}/bdm"      \
             "${BENCHMARKING_HOME}/data/${genome}/gustaf"   \
             "${BENCHMARKING_HOME}/data/${genome}/lumpy"    \
             "${BENCHMARKING_HOME}/data/${genome}/cnvnator" \
             "${BENCHMARKING_HOME}/data/${genome}/svdetect"

    log_message "working on ${genome} ... "
    for data_set in "${sets[@]}" ; do

        # only run if the dataset actually exists
        if [[ -d "${BENCHMARKING_HOME}/genomes/altered/${genome}/${data_set}" ]] ; then
            log_message "working on ${data_set} for ${genome}"

            # go over all the different coverages we expect to have for this data set
            for coverage in "${coverages[@]}" ; do
                log_message "working on ${coverage}x for ${data_set} for ${genome}"

                # general paths
                reads1_path="${BENCHMARKING_HOME}/genomes/altered/${genome}/${data_set}/reads/${coverage}x/reads_paired1.fixed.fq"
                reads2_path="${BENCHMARKING_HOME}/genomes/altered/${genome}/${data_set}/reads/${coverage}x/reads_paired2.fixed.fq"
                reference_path="${BENCHMARKING_HOME}/genomes/reference/${genome}/speedseq/reference.normalized.fa"
                alignment_path="${BENCHMARKING_HOME}/genomes/altered/${genome}/${data_set}/aligned/${coverage}x/bwa/reads_aligned.sorted.bam"

                speedseq_alignment_path="${BENCHMARKING_HOME}/genomes/altered/${genome}/${data_set}/aligned/${coverage}x/speedseq/reads_aligned.bam"
                splitters_path="${BENCHMARKING_HOME}/genomes/altered/${genome}/${data_set}/aligned/${coverage}x/speedseq/reads_aligned.splitters.bam"
                discordants_path="${BENCHMARKING_HOME}/genomes/altered/${genome}/${data_set}/aligned/${coverage}x/speedseq/reads_aligned.discordants.bam"

                # bdm specific paths
                work_dir="${BENCHMARKING_HOME}/data/${genome}/bdm/${data_set}_${coverage}x_temp"
                out_file="${BENCHMARKING_HOME}/data/${genome}/bdm/${data_set}_${coverage}x.vcf"

                log_message "running breakdancer max for ${genome} - ${data_set} @ ${coverage}x "
                time /bin/bash ${bdm} -a ${alignment_path} --working-dir ${work_dir} -o ${out_file} >> ${log_file}

                # gustaf takes too long, should be run separate in the interest of time
                # gustaf specific paths
                #work_dir="${BENCHMARKING_HOME}/data/${genome}/gustaf/${data_set}_${coverage}x_temp"
                #out_file="${BENCHMARKING_HOME}/data/${genome}/gustaf/${data_set}_${coverage}x.vcf"

                #log_message "running gustaf for ${genome} - ${data_set} @ ${coverage}x "
                #time /bin/bash ${gustaf} -r1 ${reads1_path} -r2 ${reads2_path} -ref ${reference_path} \
                #                    -o ${out_file} --working-dir ${work_dir} >> ${log_file}

                # cnvnator specific paths
                work_dir="${BENCHMARKING_HOME}/data/${genome}/cnvnator/${data_set}_${coverage}x_temp"
                out_file="${BENCHMARKING_HOME}/data/${genome}/cnvnator/${data_set}_${coverage}x.vcf"

                log_message "running cnvnator for ${genome} - ${data_set} @ ${coverage}x "
                time /bin/bash ${cnvnator} -a ${speedseq_alignment_path} -ref ${reference_path} \
                                    -o ${out_file} --working-dir ${work_dir} >> ${log_file}

                # lumpyexpress specific paths
                work_dir="${BENCHMARKING_HOME}/data/${genome}/lumpy/${data_set}_${coverage}x_temp"
                out_file="${BENCHMARKING_HOME}/data/${genome}/lumpy/${data_set}_${coverage}x.vcf"

                log_message "running lumpyexpress for ${genome} - ${data_set} @ ${coverage}x "
                time /bin/bash ${lumpy} -a ${speedseq_alignment_path} -s ${splitters_path} -d ${discordants_path} \
                                   -c ${BENCHMARKING_HOME}/code/configs/lumpyexpress.config -ref ${reference_path} \
                                   -o ${out_file} --working-dir ${work_dir} >> ${log_file}
                
            done
        fi
    done
done

