#!/bin/bash

cnvnator_bin=${CNVN_BIN}
cnvnator2vcf_bin=${CNVN_VCF}

working_dir="./temp"
prep=false
make_bam=false
clean_temp=true

POSITIONAL=()
while [[ $# -gt 0 ]]
do
    key="$1"

    case $key in
        # path to alignment file in sam/bam format
        # if sam is provided, -sam must also be set
        -a|--alignment)
        alignment_file="$2"
        shift # past argument
        shift # past value
        ;;
        # path to alignment file in sam/bam format
        # if sam is provided, -sam must also be set
        -ref|--reference-genome)
        reference_file="$2"
        shift # past argument
        shift # past value
        ;;
        # output filename
        -o|--output)
        output_file="$2"
        shift # past argument
        shift # past value
        ;;
        # working directory where temporary data is stored
        # defaults to ./temp
        --working-dir)
        working_dir="$2"
        shift # past argument
        shift # past value
        ;;
        # whether or not the alignment must be sorted 
        --prep-alignment)
        prep=true
        shift # past argument
        ;;
        # elect to not delete temporary files
        --keep-temp)
        clean_temp=false
        shift # past argument
        ;;
        # whether or not to generate reference index for bwa
        -sam|--sam-alignment)
        make_bam=true
        shift # past argument
        ;;
        *)    # unknown option
        POSITIONAL+=("$1") # save it in an array for later
        shift # past argument
        ;;
    esac
done
set -- "${POSITIONAL[@]}" # restore positional parameters

alignment_name=$(basename -- "$alignment_file")
ref_name=$(basename -- "$reference_file")
extension="${alignment_name##*.}"
extension_ref="${ref_name##*.}"

aligned_reads="aligned_reads"
ref="reference"
temp_out="cnvnator.out"

mkdir -p ${working_dir}
cp ${alignment_file} ${working_dir}/${aligned_reads}.${extension}
cp ${reference_file} ${working_dir}/${ref}.${extension_ref}

pushd ${working_dir}

if [[ ${make_bam} == true ]] ; then
    samtools view -S -b ${aligned_reads}.${extension} > ${aligned_reads}.bam
    extension=bam
fi

if [[ ${prep} == true ]] ; then
    samtools sort ${aligned_reads}.${extension} > ${aligned_reads}.sorted.${extension}
    aligned_reads=${aligned_reads}.sorted
fi

time ${cnvnator_bin} -root ${aligned_reads}.root -tree ${aligned_reads}.${extension}
time ${cnvnator_bin} -root ${aligned_reads}.root -his 1000 -fasta ${ref}.${extension_ref}
time ${cnvnator_bin} -root ${aligned_reads}.root -stat 1000
time ${cnvnator_bin} -root ${aligned_reads}.root -partition 1000
time ${cnvnator_bin} -root ${aligned_reads}.root -call 1000 > ${temp_out}
time ${cnvnator2vcf_bin} -prefix "cnvnator" -reference ${reference_file} ${temp_out} > ${output_file} 

popd

if [[ ${clean_temp} == true ]] ; then 
    rm -rf ${working_dir}
fi
