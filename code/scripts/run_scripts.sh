#!/bin/bash

scripts_dir=${BENCHMARKING_HOME}/genomes/scripts/${1}

R CMD BATCH ${scripts_dir}/all.r
R CMD BATCH ${scripts_dir}/deletions.r
R CMD BATCH ${scripts_dir}/insertions.r
R CMD BATCH ${scripts_dir}/inversions.r
R CMD BATCH ${scripts_dir}/duplications.r

