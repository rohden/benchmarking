from sys import exit
from argparse import ArgumentParser

DNA5 = set(['A', 'G', 'C', 'T', 'N'])

def gen_file_names(prefix, start, stop, extension):
    for i in range(start, stop + 1):
        yield f"{prefix}{i}.{extension}", f"{prefix}{i}.fixed.{extension}"

    return


def parse_fasta(prefix):
    try:
        parse_files(prefix=prefix, start=1, stop=2, extension='fa', gap=1, offset=0)

    except IOError:
        try:
            parse_files(prefix=prefix, start=1, stop=2, extension='fasta', gap=1, offset=0)

        except IOError:
            print(f'could not open files with prefix "{prefix}" and extension "fa" or "fasta"')
            exit(1)

    return None


def parse_fastq(prefix):
    try:
        parse_files(prefix=prefix, start=1, stop=2, extension='fq', gap=4, offset=1)

    except IOError:
        try:
            parse_files(prefix=prefix, start=1, stop=2, extension='fastq', gap=4, offset=1)

        except IOError:
            print(f'could not open files with prefix "{prefix}" and extension "fq" or "fastq"')
            exit(1)

    return None


def parse_files(prefix, start, stop, extension, gap, offset):
    for in_name, out_name in gen_file_names(prefix, start, stop, extension):
        parse_file(in_name, out_name, gap, offset)

    return None


def parse_file(in_name, out_name, gap, offset, **kwargs):
    lines = None
    
    with open(in_name, 'r') as f:
        lines = f.readlines()

    with open(out_name, 'w+') as f:
        for i, line in enumerate(lines):
            if i < offset or (gap !=1 and i % gap != offset):
                f.write(f'{line}')
                
            elif gap == 1 or i % gap == offset:
                new_line = to_dna5_format(line)
                f.write(f'{new_line}\n')

            else:
                f.write(f'{line}\n')

    return None


def to_dna5_format(line):
    new_line = []
    for c in line.strip('\n'):
        if c in DNA5:
            new_line.append(c)
        else:
            new_line.append('N')
    
    return ''.join(new_line)


def main():
    parser = ArgumentParser(prog="ART Reads Normalizer", description='Removes all non-DNA5 characters from fastq/fasta reads and replaces them with "N"')
    parser.add_argument("--prefix", "-p", required=True, help="prefix", metavar="configuration path")
    parser.add_argument("--fasta", "-fa", help="input is fasta", action="store_true") 
    parser.add_argument("--fastq", "-fq", help="input is fastq (Default)", action="store_true")
    args = parser.parse_args()

    if "prefix" not in args:
        print('no file prefix received, exiting')
        exit(args)

    if args.fasta:
        parse_fasta(args.prefix)
    else:
        parse_fastq(args.prefix)

    return None


if __name__ == "__main__":
    main()
