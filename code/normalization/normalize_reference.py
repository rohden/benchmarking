from argparse import ArgumentParser
from sys import exit

DNA5 = set(['A', 'G', 'C', 'T', 'N'])

def main():
    parser = ArgumentParser(prog="Reference normalizer", description='Removes all non-DNA5 characters from fasta formated reference genome and replace them with "N"')
    parser.add_argument("--ref", "-r", required=True, help="reference to normalize")
    args = parser.parse_args()

    if args.ref is None:
        exit(args)

    normalize_reference(args.ref)

    return None


def normalize_reference(fname):
    name_split = fname.split('.')
    new_fname = '.'.join([*name_split[:-1], 'normalized', name_split[-1]])

    with open(fname, 'r') as in_file:
        with open (new_fname, 'w+') as out_file:
            for line in in_file:
                my_line = line.strip('\n')

                if my_line.startswith('>'):
                    out_file.write(line)

                else:
                    new_line = []

                    for c in my_line:
                        if c in DNA5:
                            new_line.append(c)
                        else:
                            new_line.append('N')

                    out_file.write(f'{"".join(new_line)}\n')
                


    return None


if __name__ == "__main__":
    main()
