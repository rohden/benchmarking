from datetime import datetime
import sys

def write_to_stderr(msg):
    print(msg, file=sys.stderr)
    return None


class Logger:
    def __init__(self, **kwargs):
        self.off = {0, 'off', 'o', 'silent', 's', 'quiet', 'q', 'stfu'}
        self.crit = {1, 'critical', 'c', 'error', 'e'}
        self.info = {2, 'all', 'a', 'info', 'i', 'debug', 'd'}

        self.logstream = write_to_stderr if 'logstream' not in kwargs else kwargs['logstream']
        self.verbose = True if 'verbose' not in kwargs else kwargs['verbose']
        if 'loglevel' not in kwargs:
            # turn off output if user turned it off
            self.level = 1 if self.verbose else 0

        # skip check if user turned it off
        elif self.verbose:
            loglevel = kwargs['loglevel'].lower()

            if loglevel in self.off:
                self.level = 0
                self.verbose = False
            elif loglevel in self.crit:
                self.level = 1
            elif loglevel in self.info:
                self.level = 2
            else:
                error_msg = [
                    f"Option \"{kwargs['loglevel']}\" is not a valid loglevel configuration.",
                    f"\tUse one of {self.crit} for error-only logging [default]",
                    f"\tUse one of {self.info} for error and info logging",
                    f"\tUse one of {self.off} to turn off all logging."
                ]

                raise ValueError('\n'.join(error_msg))
    
        return


    def log_message(self, msg, level):
        def flatten_str(s):
            if isinstance(s, str):
                return s.lower()
            return s

        if level not in self.crit and level not in self.info:
            raise ValueError(f'Received invalid log level "{level}"')

        log_format = "[{LEVEL}] -- {TIMESTAMP}: {MSG}"
        if self.verbose:
            lvl = flatten_str(level)
            my_level = (1, 'ERROR') if lvl in self.crit else (2, 'INFO')

            if self.level >= my_level[0]:
                my_timestamp = datetime.now().strftime("%Y-%m-%d %X")
                self.logstream(log_format.format(TIMESTAMP=my_timestamp, LEVEL=my_level[1], MSG=msg))
        return
