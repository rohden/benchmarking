import csv
from datetime import datetime
from logger import Logger

class BDM_Reader:
    def __init__(self, **kwargs):
        self.__header = None
        self.metadata = None
        self.data = None
        self.logger = Logger(**kwargs)
        self.logger.log_message(msg=f'BDM_Reader setup completed!', level='info')

        return


    def parse(self, fname):
        self.logger.log_message(msg=f'Attempting to parse breakdancer max file "{fname}"', level='info')

        try:
            with open(fname, 'r') as f:
                self.__set_header(f)
                self.__set_meta(f)

                f.seek(self.__header)
                dr = csv.DictReader(f, dialect='excel-tab')
                self.data = [dict(l) for l in dr]
                self.data.sort(key=lambda x: x['Pos1'])

        except IOError as e:
            self.logger.log_message(msg=f'Could not parse "{fname}" due to error:\n{str(e)}', level='error')

        return


    def __set_meta(self, f):
        start = f.tell()
        self.metadata = {}

        if self.__header is None:
            self.logger.log_message(msg=f'Header not set before calling set_meta, calling set_header first ...', level='info')
            self.__set_header(f)

        f.seek(0)

        while True:
            # read until the header is reached or an uncommented line is found
            if f.tell() < self.__header - 1:
                line = f.readline()

                if not line.startswith('#'):
                    break
            else:
                break

            self.logger.log_message(msg=f'Metadata found: "{line}"', level='info')

            # Library Statistics are on the next line unlike all others
            if not line.startswith('#Library Statistics'):
                data = line.split(':')
                self.metadata[data[0][1:].lower()] = data[1]

            else:
                data = f.readline()[1:].rstrip('\n').split('\t')
                # the alignment filename doesn't have a key 
                # all others are in 'k:v' format 
                self.metadata['statistics'] = dict(i.split(':') for i in data[1:])
                self.metadata['statistics']['alignment-file'] = data[0]

        self.logger.log_message(msg=f'Done processing metadata found:\n{self.metadata}', level='info')

        f.seek(start)
        return 


    def __set_header(self, f):
        start = f.tell()
        f.seek(0)
        self.__header = 0

        while True:
            # add one to discard leading pound symbol
            new_head = f.tell() + 1
            line = f.readline()

            if not line.startswith('#'):
                break

            self.__header = new_head
        
        f.seek(self.__header)
        header = f.readline().rstrip("\n").replace("\t", ",")
        f.seek(start)

        self.logger.log_message(msg=f'Header found at byte {self.__header}', level='info')
        self.logger.log_message(msg=f'Header line is:"{header}"', level='info')

        return 


    def __iter__(self):
        return iter(self.data)


    def __len__(self):
        return len(self.data)