import csv
import re

from datetime import datetime
from logger import Logger

class SVD_Reader:
    def __init__(self, **kwargs):
        self.data = None
        self.logger = Logger(**kwargs)
        self.logger.log_message(msg=f'SVD_Reader setup completed!', level='info')

        return


    def parse(self, fname):
        self.logger.log_message(msg=f'Attempting to parse SVDetect file "{fname}"', level='info')

        try:
            with open(fname, 'r') as f:
                dr = csv.DictReader(f, dialect='excel-tab')
                self.data = self.__parse_lines(dr)
                self.data.sort(key=lambda x: x['start1'])

        except IOError as e:
            self.logger.log_message(msg=f'Could not parse "{fname}" due to error:\n{str(e)}', level='error')

        return


    def __parse_lines(self, dr):
        lines = []
        for d in dr:
            new_line = []
            for l in d:
                # special case for 'prefix_start-end' ranges
                if re.match('.*_.*-.*', l) and re.match('.*-.*', d[l]):
                    keys = l.rstrip('\n').split('-')
                    vals = d[l].rstrip('\n').split('-')

                    prefix = keys[0].split('_')[0]
                    keys[1] = f'{prefix}_{keys[1]}'
                    for k, v in zip(keys, vals):
                        new_line.append((k, v))

                # special case for 'start-end' ranges
                elif re.match('.*-.*', l) and re.match('.*-.*', d[l]):
                    keys = l.rstrip('\n').split('-')
                    vals = d[l].rstrip('\n').split('-')
                    for k, v in zip(keys, vals):
                        new_line.append((k, v))

                # simple k-v pair
                else:
                    new_line.append((l, d[l]))
            
            lines.append(dict(new_line))

        return lines


    def __iter__(self):
        return iter(self.data)


    def __len__(self):
        return len(self.data)
