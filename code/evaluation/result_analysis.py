import toml
import glob

from rsvsim_reader import RSVSim_Reader
from svd_reader import SVD_Reader
from bdm_reader import BDM_Reader
from vcf import Reader as VCF_Reader
from argparse import ArgumentParser
from logger import Logger
from pathlib import Path

def get_sv_type(custom_tuple):
    return custom_tuple.to_tuple()[1]


def is_same_sv_type(actual, expected, kind):
    bdm_kinds = {
        'del': ['DEL'],
        'dup': [None],
        'ins': ['INS'],
        'inv': ['INV'],
        'xlo': ['ITX', 'CTX']
    }
    svd_kinds = {
        'del': ['DELETION'],
        'dup': ['DUPLICATION', 'LARGE_DUPLI', 'SMALL_DUPLI', 'INV_DUPLI', 'COAMPLICON', 'INV_COAMPLICON'],
        'ins': ['INSERTION', 'INS_FRAGMT', 'INV_INS_FRAGMT'],
        'inv': ['INVERSION', 'INV_FRAGMT', 'INV_INS_FRAGMT', 'INV_DUPLI'],
        'xlo': ['TRANSLOC', 'INV_TRANSLOC']
    }
    vcf_kinds = {
        'del': ['DEL', 'CNV'],
        'dup': ['DUP', 'CNV'],
        'ins': ['INS'],
        'inv': ['INV'],
        'xlo': [None]
    }

    expected_sv_type = get_sv_type(expected)

    if kind == 'bdm':
        return bdm_kinds[expected_sv_type] is not None and actual['Type'] in bdm_kinds[expected_sv_type]

    elif kind == 'svd':
        return svd_kinds[expected_sv_type] is not None and actual['SV_type'] in svd_kinds[expected_sv_type]

    elif kind == 'vcf':
        return vcf_kinds[expected_sv_type] is not None and actual.INFO['SVTYPE'] in vcf_kinds[expected_sv_type]

    else:
        raise ValueError(f'The format "{kind}" is not a supported output format -- expected "bdm" or "svd" or "vcf"')
    
    return


def parse_conf(conf_path, logger):
    with open(conf_path, 'r') as f:
        c = toml.load(f)

        if 'general' not in c:
            exit(1)
        elif 'tools' not in c['general']:
            exit(1)
        elif 'output' not in c['general']:
            exit(1)
        elif 'true_positives' not in c['general']:
            exit(1)
        
        for k in c['general']['tools']:
            if k not in c:
                exit(1)
            elif 'format' not in c[k]:
                exit(1)
            elif 'path' not in c[k]:
                exit(1)
            elif 'prefixes' not in c[k]:
                exit(1)
            elif 'output_name' not in c[k]:
                exit(1)
            elif 'output' not in c[k]:
                c[k]['output'] = c['general']['output']

    return c


def get_tp_reader(tp_dir, logger):
    my_path = tp_dir / '*.csv'
    names = glob.glob(str(my_path))

    r = RSVSim_Reader()

    for name in names:
        logger.log_message(msg=f'Extracting true positive information from {name} ... ', level='info')
        r.parse(name, prefix=f'{name}')

    return r


def get_results(file_format, files, tp_data, thresholds, logger):
    if file_format == 'bdm':
        results = get_results_bdm(files, tp_data, thresholds, logger)
    elif file_format == 'svd':
        results = get_results_svd(files, tp_data, thresholds, logger)
    elif file_format == 'vcf':
        results = get_results_vcf(files, tp_data, thresholds, logger)
    else:
        raise ValueError(f'Unknown result format "{file_format}" -- was expeciting "vcf", "bdm", or "svd"')

    return results


def get_results_bdm(files, tp_data, thresholds, logger):
    results = {
        f:[
            {
                'true_positives': 0, 
                'false_positives': 0, 
                'false_negatives': tp_data.variant_count(), 
                'expected_calls': tp_data.variant_count(), 
                'total_calls': 0
            } for _ in thresholds
        ] for f in files
    }

    for fname in files:
        logger.log_message(msg=f'Working on {fname} for BDM ... ', level='info')
        r = BDM_Reader()
        r.parse(fname)
        seen = [set() for _ in thresholds]

        for item in r:
            for i, _ in enumerate(thresholds): 
                results[fname][i]['total_calls'] = len(r)

            for i, threshold in enumerate(thresholds):
                hits = tp_data.find(int(item['Pos1']), threshold)

                # only accept if end position and type also match
                if hits:
                    for hit in hits:
                        if hit.to_tuple()[2] not in seen[i] and is_same_sv_type(item, hit, 'bdm'):
                            results[fname][i]['true_positives'] += 1
                            results[fname][i]['false_negatives'] -= 1
                            seen[i].add(hit.to_tuple()[2])
                            break
                    else:
                        results[fname][i]['false_positives'] += 1


                else:
                    results[fname][i]['false_positives'] += 1

    return results


def get_results_svd(files, tp_data, thresholds, logger):
    results = {
        f:[
            {
                'true_positives': 0, 
                'false_positives': 0, 
                'false_negatives': tp_data.variant_count(), 
                'expected_calls': tp_data.variant_count(), 
                'total_calls': 0
            } for _ in thresholds
        ] for f in files
    }

    for fname in files:
        logger.log_message(msg=f'Working on {fname} for SVD ... ', level='info')
        r = SVD_Reader()
        r.parse(fname)
        seen = [set() for _ in thresholds]

        for item in r:
            for i, _ in enumerate(thresholds): 
                results[fname][i]['total_calls'] = len(r)
                
            for i, threshold in enumerate(thresholds):
                hits = tp_data.find(int(item['start1']), threshold)

                # only accept if end position and type also match
                if hits:
                    for hit in hits:
                        if hit.to_tuple()[2] not in seen[i] and is_same_sv_type(item, hit, 'svd'):
                            results[fname][i]['true_positives'] += 1
                            results[fname][i]['false_negatives'] -= 1
                            seen[i].add(hit.to_tuple()[2])
                            break
                    else:
                        results[fname][i]['false_positives'] += 1

                else:
                    results[fname][i]['false_positives'] += 1

    return results


def get_results_vcf(files, tp_data, thresholds, logger):
    results = {
        f:[
            {
                'true_positives': 0, 
                'false_positives': 0, 
                'false_negatives': tp_data.variant_count(), 
                'expected_calls': tp_data.variant_count(), 
                'total_calls': 0
            } for _ in thresholds
        ] for f in files
    }

    for fname in files:
        logger.log_message(msg=f'Working on {fname} for VCF ... ', level='info')
        with open(fname, 'r') as in_file:
            r = VCF_Reader(fsock=in_file)
            seen = [set() for _ in thresholds]

            for item in r:
                for i, threshold in enumerate(thresholds):
                    results[fname][i]['total_calls'] += 1
                    hits = tp_data.find(int(item.POS), threshold)

                    # only accept if end position and type also match
                    if hits:
                        for hit in hits:
                            if hit.to_tuple()[2] not in seen[i] and is_same_sv_type(item, hit, 'vcf'):
                                results[fname][i]['true_positives'] += 1
                                results[fname][i]['false_negatives'] -= 1
                                seen[i].add(hit.to_tuple()[2])
                                break

                    else:
                        results[fname][i]['false_positives'] += 1

    return results


def calculate_stats(data, logger):
    for fname, dataset in data.items():
        logger.log_message(msg=f'Working on {fname} stats ... ', level='info')

        for i, val in enumerate(dataset):
            logger.log_message(msg=f"True positives: {val['true_positives']}", level='info')
            logger.log_message(msg=f"False positives: {val['false_positives']}", level='info')
            logger.log_message(msg=f"False negatives: {val['false_negatives']}", level='info')
            data[fname][i]['precision'] = 0 if (val['false_positives'] + val['true_positives']) == 0 else val['true_positives'] / (val['false_positives'] + val['true_positives'])
            data[fname][i]['recall'] = val['true_positives'] / (val['false_negatives'] + val['true_positives'])


    return


def output_results(out_path, data, thresholds, logger):
    column_offset = "".join([',' for _ in range(1, len(thresholds))])
    thresholds_str = ','.join(f'{t}' for t in thresholds)
    header = [
        f'filename, Precision, {column_offset} Recall, {column_offset} Expected Calls, {column_offset} Total Calls',
        f',{thresholds_str},{thresholds_str},{thresholds_str},{thresholds_str}\n'
    ]

    with open(out_path, 'w+') as outfile:
        logger.log_message(msg=f'Writing results to {str(out_path)} ...', level='info')
        outfile.write('\n'.join(header))
        
        for fname, dataset in data.items():
            line = [fname]
            PREC = []
            REC = []
            TC = []
            EC = []
            for val in dataset:
                PREC.append(str(val['precision']))
                REC.append(str(val['recall']))
                TC.append(str(val['total_calls']))
                EC.append(str(val['expected_calls']))

            line.extend([*PREC, *REC, *EC, *TC, '\n'])
            outfile.write(','.join(line))        
        
        logger.log_message(msg=f'Done writing results to {str(out_path)}', level='info')


    return


def process_results(config, tp_folder, thresholds, logger):
    tp = Path(tp_folder)
    data_matrix = {}

    logger.log_message(msg=f'Processing results ...', level='info')

    for prefix in config['prefixes']:
        logger.log_message(msg=f'Working on {prefix} ...', level='info')

        tp_files = tp / prefix
        tp_data = get_tp_reader(tp_files, logger)

        data_files = Path(config['path']) / f'{prefix}*'
        files = glob.glob(str(data_files))

        results = get_results(config['format'], files, tp_data, thresholds, logger)
        data_matrix.update(**results)

        logger.log_message(msg=f'Done with {prefix}', level='info')


    logger.log_message(msg=f'Calculating stats ...', level='info')
    
    calculate_stats(data_matrix, logger)
    
    logger.log_message(msg=f'Writing stats to file ...', level='info')
    out_path = Path(config['output']) / config['output_name']
    output_results(out_path, data_matrix, thresholds, logger)
    
    return


def main():
    parser = ArgumentParser()
    parser.add_argument("--conf", "-c", required=True, help="path to the (TOML-formatted) configuration file", metavar="configuration path") 
    parser.add_argument("--verbose", "-v", help="print status messages to stdout", action="store_true")
    args = parser.parse_args()

    log_config = {
        'verbose': True, 
        'loglevel': 'error' if args.verbose is None else 'info'
    }

    logger = Logger(**log_config)

    logger.log_message(msg='Parsing configuration file ...', level='info')
    conf = parse_conf(args.conf, logger)

    for tool in conf['general']['tools']:
        logger.log_message(msg=f'Working on {tool} ...', level='info')

        process_results(conf[tool], conf['general']['true_positives'], conf['general']['thresholds'], logger)

        logger.log_message(msg=f'Finished with {tool}', level='info')

    logger.log_message(msg=f'Done.', level='info')

    return


if __name__ == '__main__':
    main()
